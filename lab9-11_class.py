class PlotBuilder:
    '''
    Класс для построения графиков
    '''
    __er_rate = 10 ** (-10)
    __x = []
    __y = []
    __middle_x = float()
    __middle_y = float()

    def __init__(self, x=[], y=[]):
        '''
        Инициализатор объекта
        '''
        self.__x = x
        self.__y = y
        self.__SetMiddle()

    def SetCoords(self, x=[], y=[]):
        '''
        Задаем координаты
        '''
        self.__x = x
        self.__y = y
        self.__SetMiddle()

    def GetCoords(self):
        '''
        Получаем координаты
        '''
        return self.__x, self.__y

    def __SetMiddle(self):
        '''
        Находим середину фигуры
        '''
        if self.__x != [] and self.__y != []:
            self.__figure_middle(self.__x, self.__y)
        else:
            self.__middle_x = 0
            self.__middle_y = 0

    def __figure_middle(self, x, y):
        '''
        Находим середину фигуры
        '''
        while not (self.__is_calculated(x) and self.__is_calculated(y)):
            x = self.__middle(x)
            y = self.__middle(y)
        self.__middle_x = sum(x) / len(x)
        self.__middle_y = sum(y) / len(y)

    def __middle(self, a):
        '''
        Находим середины сторон фигуры
        '''
        return [((a[i] + a[(i + 1) % len(a)]) / 2) for i in range(len(a))]

    def __is_calculated(self, a):
        '''
        Проверяем, не больше ли разница элементов, чем погрешность
        '''
        for i in range(1, len(a)):
            if abs(a[i - 1] - a[i % len(a)]) > self.__er_rate:
                return False
        return True

    def Build(self):
        '''
        Строим график
        '''
        from matplotlib import pyplot as plt
        x = self.__x
        y = self.__y
        x.append(x[0])
        y.append(y[0])
        plt.plot(x, y, 'b')
        plt.plot(self.__middle_x, self.__middle_y, 'rx')
        plt.show()

class FileReader:
    __path = str()

    def __init__(self, path):
        self.__path = path

    def ReadFile(self):
        x = list()
        y = list()
        with open(self.__path, 'r') as file:
            for line in file:
                XY = list(map(int, line.replace(';', '').split()))
                x.append(XY[0])
                y.append(XY[1])
        return x, y

    def WriteFile(self):
        pass

from FileReader import FileReader
from PlotBuilder import PlotBuilder

path = 'data.csv'

x, y = FileReader(path).ReadFile()
plot = PlotBuilder(x, y)
plot.Build()