import random

print('Значения переменных:')
x = [random.randint(-10, 10) for i in range(10)]
y = [random.randint(-10, 10) for i in range(10)]
for i in range(10):
    print('x =', x[i], 'y =', y[i])

file = open('Значения координат.txt', 'w')
for i in range(10):
    file.write(str(x[i]) + ' ' + str(y[i]) + '\n')
file.close()

x = []
y = []
file = open('Значения координат.txt', 'r')
for i in range(10):
    c = map(int, file.readline().split())
    c = list(c)
    x.append(c[0])
    y.append(c[1])
file.close()

if random.randint(0, 1)==0:
    x[random.randint(0,9)]=random.randint(0, 10)
else:
    y[random.randint(0, 9)]=random.randint(0, 10)
print('Измененное значение')
for i in range(10):
    print('x =', x[i], 'y =', y[i])